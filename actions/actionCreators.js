export function getBooks(data) {
  return {
    type: 'GET_BOOKS',
    data
  };
}
export function addBook(name,email,contact){
	return {
		type : 'ADD_BOOK',
		name,
		email,
		contact
	};
}
export function deleteBook(i){
	return{
		type : 'DELETE_BOOK',
		i
	};
}
export function editBook(i){
	return{
		type : 'EDIT_BOOK',
		i
	};
}