function bookReducer(state=[], action) {
  if(state === undefined)
    return [];
  switch (action.type) {
    case 'GET_BOOKS' :
    console.log("GET_BOOKS ",state,action);
      return  [...action.payload.data]
    case 'ADD_BOOK' :
        console.log("Add_BOOKS ",state,action);
      return [
          ...state,
          {
            id: action.payload._id,
            name : action.payload.name,
            email : action.payload.email,
            contact : action.payload.contact
          }
        ];
    case 'DELETE_BOOK' :
    console.log("Delete_BOOKS ",state,action);
      return [
        ...state.slice(0, action.payload),
        ...state.slice(action.payload+1)
      ];
    case 'EDIT_BOOK' :
    console.log("Edit_BOOKS ",state,action);
      return state;
    default:
      return state;
  }
}

export default bookReducer;
