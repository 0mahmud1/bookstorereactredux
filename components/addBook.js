import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import store from '../store';
import { Router, Route, Link, browserHistory, IndexRoute  } from 'react-router';
var addBook = React.createClass({
	handleSubmit : function(e){
		e.preventDefault();
		//console.log("name ",this.refs.name.value );
		var data = {
			name : this.refs.name.value,
			email : this.refs.email.value,
			contact : this.refs.contact.value
		};
		axios.post("http://localhost:3001/app",data)
		.then(function(response){
			console.log("Response = ",response.data);
			store.dispatch({
				type :'ADD_BOOK',
				payload: response.data
			})
			console.log("Store at add state ",store.getState());
			//location.href = '/';
		})
		.catch(function(err){
			console.log("Eroor =",err);
		})

	},
	render(){
		return(
			<div>
		      <form className="form-horizontal" onSubmit={this.handleSubmit}>
		              <div className="form-group">
		                <label className="control-label col-sm-4 lbl" >Username  :</label>
		                <div className="col-sm-8">
		                    <input type='text' ref='name'/>
		                </div>
		              </div>
		              <div className="form-group">
		                <label className="control-label col-sm-4 lbl">Email  :</label>
		                <div className="col-sm-8">
		                    <input type='text' ref='email' />
		                </div>
		              </div>
		              <div className="form-group">
		                <label className="control-label col-sm-4 lbl">Contact  :</label>
		                <div className="col-sm-8">
		                    <input type='text' ref='contact' />
		                </div>
		              </div>
		              <div className="form-group">
		                <div className="col-sm-offset-4">
		                  <input type='submit' value='Add'/>
		                </div>
		              </div>
		      </form>
		     </div>
		);
	}
});
export default addBook;