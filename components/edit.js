import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import store from '../store';

var edit = React.createClass({
	getInitialState : function(){
		const book = this.props.books[this.props.params.id];
		return {
			name : book.name,
			email : book.email,
			contact : book.contact
		}
	},
	handleSubmit : function(e){
		e.preventDefault();
		var data = {
			name : this.state.name,
			email : this.state.email,
			contact : this.state.contact 
		};
		console.log("Edit Data ",data);
		var index = this.props.params.id;
		console.log("index ",index); 
		this.hreflink = "http://localhost:3001/app/"+this.refs.id.value;
		axios.put(this.hreflink,data)
		.then(function(response){
			console.log("Response = ",response.data);
			store.dispatch({
				type :'EDIT_BOOK',
				payload: index 
			})
			console.log("Store at add state ",store.getState());
			//location.href = '/';
		})
		.catch(function(err){
			console.log("Eroor =",err);
		})
	},
  nameChange: function(e){
    this.setState({
      name: e.target.value
    });
  },
   emailChange: function(e){
    this.setState({
      email: e.target.value
    });
  },
  contactChange: function(e){
    this.setState({
      contact: e.target.value
    });
  }, 
	render(){
const book = this.props.books[this.props.params.id];
		return(
			<div>
		      <form className="form-horizontal" onSubmit={this.handleSubmit}>
		              <div className="form-group">
		                <label className="control-label col-sm-4 lbl" >Username  :</label>
		                <div className="col-sm-8">
		                    <input type='text' ref='name' onChange={this.nameChange} value={this.state.name}/>
		                </div>
		              </div>
		              <div className="form-group">
		                <label className="control-label col-sm-4 lbl">Email  :</label>
		                <div className="col-sm-8">
		                    <input type='text' ref='email' onChange={this.emailChange} value={this.state.email}/>
		                </div>
		              </div>
		              <div className="form-group">
		                <label className="control-label col-sm-4 lbl">Contact  :</label>
		                <div className="col-sm-8">
		                    <input type='text' ref='contact' onChange={this.contactChange} value={this.state.contact}/>
		                </div>
		              </div>
		              <input type='hidden' ref='id' value={book._id}/>
		              <div className="form-group">
		                <div className="col-sm-offset-4">
		                  <input type='submit' value='Edit'/>
		                </div>
		              </div>
		      </form>
		     </div>
		);
	}
});
export default edit;