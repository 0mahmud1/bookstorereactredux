import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import store from '../store';
import { Router, Route, Link, browserHistory, IndexRoute  } from 'react-router';
var bookList = React.createClass({
	componentDidMount : function(){
		axios.get("http://localhost:3001/app")
		.then(function(response){
			console.log("Response = ",response.data);
			store.dispatch({
				type :'GET_BOOKS',
				payload : response
			})
			console.log("Store state ",store.getState());
		})
		.catch(function(err){
			console.log("Eroor =",err);
		})
	},
	deleteItem : function(id,i){

	    console.log("call delete ",id);
	    this.hreflink = "http://localhost:3001/app/book/"+id;
	    axios.delete(this.hreflink)
	    .then(function(res){
	    	console.log("Delete Response ",res , id,i);
	    	store.dispatch({
	    		type : 'DELETE_BOOK',
	    		payload : i
	    	})
	    	console.log("Store delete state ",store.getState());
	    	//location.href = '/';
	    })
	    .catch(function(err){
	    	console.log("Delete Error ",err);
	    })
    
  	},
  	
	render(){
		return(
			<div>
			<h1>Book List</h1>
             <table className="table table-bordered">
              <thead><tr><td>Name</td><td>Email</td><td>Contact</td></tr></thead>
              <tbody>
                {this.props.books.map((result,i) => (
                  <tr key={i}>
                    <td>{result.name}</td>
                    <td>{result.email}</td>
                    <td>{result.contact}</td>
                    <td>
                    	<input type="button" onClick={this.deleteItem.bind(this,result._id,i)} value="Delete"/>
                    	<Link to={`/edit/${i}`}>Edit</Link>
                    </td>
                  </tr>
                ))}
              </tbody>
              </table> 
              </div>
		);
	}
});
export default bookList;